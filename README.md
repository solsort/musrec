# Museum Recommender

Possible dimensions/model:

- toggle: same creator next/prev
- weight: same period + geography
- weight:NN creator-genre-model with limited popular creators
- weight:NN image-collection + autoencode
- weight:NN combined model – all data multilabel
- ¿Placering på museum?

# Tasks

- further research into existing approaches
- identify features for training sets
- prepare data for trainingset1: smk(44K) + moma(<139K) + metropolitan(<500K)
- prepare data for trainingset2: natmus (500K)
- prepare data for trainingset3: bib+cover (<1200K)

# Approach

- identify features, i.e. artists, exhibitions, tags, period, ...
- train NN with features
- autoencode / dimension reduce  penultimate layer X data set
- use autoencode PCA or similar dimension reduction as space for distance 

# Sources
start with: smk+moma+met , bib/covers, natmus
## Natmus
dump in gitlab

## SMK
code for fetching in kunstskat code

## Moma

https://github.com/MuseumofModernArt/collection
https://github.com/MuseumofModernArt/exhibitions

## Metropolitan museum 

https://github.com/metmuseum/openaccess

## Rijksmuseum + Europeana

Europeana Data Model RDF

## Danske museer via SLKS

## Tyske museer via https://digital.deutsches-museum.de/

- search in web

# Targets
## SMK
## NatMus
## kulturdata.dk

# Notes
Possible sources:

- Nationalmuseet
- Statens Museum for Kunst
- Tyske museer
- Rijksmuseum?
- moma? 
- samtlige danske via slots og kulturstyrelsen?
- europeana
- search for large open museum data

Targets:

- smk
- natmus
- bibliotek-metadata

Notes:
https://towardsdatascience.com/similar-images-recommendations-using-fastai-and-annoy-16d6ceb3b809
https://github.com/crack00ns/Similar-Image-Recommender
https://towardsdatascience.com/finding-similar-images-using-deep-learning-and-locality-sensitive-hashing-9528afee02f5

