mkdir data
cd data
curl https://gitlab.com/solsort/natmus-images/-/raw/master/natmus.jsonl.gz | gunzip > natmus.jsonl
wget https://github.com/MuseumofModernArt/collection/raw/master/Artists.json -O moma-artists.json
wget https://github.com/MuseumofModernArt/collection/raw/master/Artworks.json -O moma-artworks.json
wget https://github.com/metmuseum/openaccess/raw/master/MetObjects.csv
curl https://gitlab.com/solsort/kunstskat/-/raw/master/data/smk.jsonl.gz | gunzip > smk.jsonl
scp bibdata.dk:bibliography.jsonl.bz2 . && bunzip2 bibliography.jsonl.bz2

#wget https://scan.rijkskoha.nl/adlibdumps/202001-rma-edm-collection.zip
